const htmlWebpackTemplate = require('html-webpack-template');

const htmlWebpackPluginConfig = ({ filename, chunks, title, scripts }) => ({
  filename,
  chunks,
  title,
  template: htmlWebpackTemplate,
  appMountId: 'root',
  xhtml: true,
  mobile: true,
  inject: false,
  minify: {
    useShortDoctype: true,
    keepClosingSlash: true,
    collapseWhitespace: true,
    preserveLineBreaks: true
  },
  scripts
});

module.exports = {
  htmlWebpackPluginConfig
};
