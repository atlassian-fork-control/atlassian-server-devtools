import { createReducer } from 'redux-create-reducer';

import {
  HIGHLIGHT,
  UNHIGHLIGHT,
  SCROLL_TO_NODE,
  TOGGLE_HIGHLIGHT_DEPRECATED_ELEMENTS
} from '../constants/highligthing';

import { CLEAR_DEPRECATIONS } from '../constants/deprecations';

import {
  highlightNode,
  unhighlightNode,
  scrollToNode,
  toggleHighlightNodes,
  removeHighlightingOnAllNodes
} from '../topics/messages/highlithing';

const defaultState = {
  highlightDeprecatedElements: false
};

const highlighting = createReducer(defaultState, {
  [TOGGLE_HIGHLIGHT_DEPRECATED_ELEMENTS](state) {
    const shouldHighlight = !state.highlightDeprecatedElements;

    toggleHighlightNodes(shouldHighlight);

    return {
      ...state,
      highlightDeprecatedElements: shouldHighlight
    };
  },

  [HIGHLIGHT](state, action) {
    const { nodeId } = action;

    highlightNode(nodeId);

    return state;
  },

  [UNHIGHLIGHT](state, action) {
    const { nodeId } = action;

    if (!state.highlightDeprecatedElements) {
      unhighlightNode(nodeId);
    }

    return state;
  },

  [SCROLL_TO_NODE](state, action) {
    const { nodeId } = action;

    scrollToNode(nodeId);

    return state;
  },

  [CLEAR_DEPRECATIONS](state) {
    removeHighlightingOnAllNodes();

    return state;
  }
});

export default highlighting;
