import React from 'react';

import styles from './EmptyDeprecationsList.less';

const EmptyDeprecationsList = () => (
  <div className={styles.EmptyDeprecationsList}>
    <p>Watching for the usage of deprecated <strong>AUI HTML patterns</strong> on the page…</p>
    <p>
      When the deprecated <strong>HTML pattern</strong> will be used, you will see here detailed information about the
      depreciation and what DOM element is affected.
    </p>
  </div>
);

export default EmptyDeprecationsList;
