import React from 'react';
import PropTypes from 'prop-types';

import styles from './PanelContent.less';

const PanelContent = ({ children }) => (
  <div className={styles.PanelContent}>
    {children}
  </div>
);

PanelContent.propTypes = {
  children: PropTypes.node
};

PanelContent.defaultProps = {
  children: null
};

export default PanelContent;
