import React from 'react';

import ToolbarItem from '../ToolbarItem/ToolbarItem';
import styles from './ToolbarDivider.less';

const ToolbarDivider = () => (
  <ToolbarItem className={styles.ToolbarDivider} />
);

export default ToolbarDivider;
