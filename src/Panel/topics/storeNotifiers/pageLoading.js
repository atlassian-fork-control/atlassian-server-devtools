import { LOADING } from '../contants';
import { isLoading } from '../../actions/panel';

const pageLoading = (attachNotifier, store) => {
  console.debug('P: Notifier: Registering "pageLoading" notifier');

  attachNotifier(LOADING, () => {
    console.debug('P: Notifier: Dispatching "pageLoading" action');

    store.dispatch(isLoading());
  });
};

export default pageLoading;
