export const VERSION = 'VERSION';
export const LOADING = 'LOADING';
export const LOADED = 'LOADED';
export const HAS_AUI = 'HAS_AUI';
export const TOGGLE_SHOW_TIMESTAMPS = 'TOGGLE_SHOW_TIMESTAMPS';
export const TOGGLE_PRESERVE_LOG = 'TOGGLE_PRESERVE_LOG';
