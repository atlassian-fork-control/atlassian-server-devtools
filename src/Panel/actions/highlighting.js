import {
  HIGHLIGHT,
  UNHIGHLIGHT,
  TOGGLE_HIGHLIGHT_DEPRECATED_ELEMENTS,
  SCROLL_TO_NODE
} from '../constants/highligthing';

export const highlightNode = nodeId => ({
  type: HIGHLIGHT,
  nodeId
});

export const unhighlightNode = nodeId => ({
  type: UNHIGHLIGHT,
  nodeId
});

export const toggleHighlightDeprecatedElements = () => ({
  type: TOGGLE_HIGHLIGHT_DEPRECATED_ELEMENTS
});

export const scrollToNode = nodeId => ({
  type: SCROLL_TO_NODE,
  nodeId
});
