import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

import './PanelApp.css';

import Panel from '../Panel/Panel';

const PanelApp = ({ store }) => (
  <Provider store={store}>
    <Panel />
  </Provider>
);

PanelApp.propTypes = {
  // eslint-disable-next-line
  store: PropTypes.object.isRequired
};

export default PanelApp;
