import retrieveVariablesValue from './retrieveVarValue';

const varName = 'window.AJS.version';

const getAuiVersion = () => {
  const results = retrieveVariablesValue(varName);
  const auiVersion = results[varName];

  return auiVersion;
};

export default getAuiVersion;
