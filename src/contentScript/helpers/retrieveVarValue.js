import camelCase from 'lodash/camelCase';

const formatVariableName = (id, variable) => camelCase(`${id}-${variable}`);

const retrieveVariablesValue = (...variables) => {
  const result = {};
  const id = Date.now().toString(32);

  let scriptContent = '';

  variables.forEach((variable) => {
    scriptContent +=
      `try {
        if (typeof (${variable}) !== 'undefined') {
          document.body.dataset['${formatVariableName(id, variable)}'] = (${variable})
        }
      } catch(e) {}`;
  });

  const script = document.createElement('script');

  script.id = id;
  script.appendChild(document.createTextNode(scriptContent));
  (document.body || document.head || document.documentElement).appendChild(script);

  variables.forEach((variable) => {
    const dataKey = formatVariableName(id, variable);
    const value = document.body.dataset[dataKey];

    if (value) {
      result[variable] = value;

      delete document.body.dataset[dataKey];
    }
  });

  script.remove();

  return result;
};

export default retrieveVariablesValue;

