import { getNodeId, storeNode } from '../deprecations/registry';
import nodeHydrate from '../helpers/nodeHydrate';
import { emit } from '../portConnection';

const deprecationNotifier = (deprecation) => {
  const { node } = deprecation;
  const nodeId = getNodeId(node);

  storeNode(nodeId, node);

  emit('DEPRECATION', {
    ...deprecation,
    nodeId,
    timestamp: Date.now(),
    node: nodeHydrate(node)
  });
};

export default deprecationNotifier;
