import listeners from './listeners/listeners';
import { on } from './portConnection';

export default () => (
  listeners.forEach(attachListener => attachListener(on))
);
