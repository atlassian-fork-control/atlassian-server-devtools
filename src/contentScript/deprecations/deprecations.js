import { initWatcher, registerSelector } from './watcher';
import getCssSelectors from './getCssSelectors';

const initDeprecationWatcher = (auiVersion, notifier) => {
  console.debug('CS: Registering deprecation watcher');

  const selectors = getCssSelectors(auiVersion);

  selectors.forEach(registerSelector);
  initWatcher(notifier);
};

export default initDeprecationWatcher;
