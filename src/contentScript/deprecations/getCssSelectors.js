import cssDeprecations from './cssDeprecations.json';

const versions = Object.keys(cssDeprecations);

const getCssSelectors = (auiVersion) => {
  const applicableVersions = versions;

  return applicableVersions
    .map(version => (
      cssDeprecations[version]
        .map(deprecation => ({
          ...deprecation,
          version,
          auiVersion
        }))
    ))
    .reduce((result, selectors) => (
      result.concat(selectors)
    ), []);
};

export default getCssSelectors;
