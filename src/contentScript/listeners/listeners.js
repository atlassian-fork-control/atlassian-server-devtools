import devtoolsClosed from './devtoolsClosed';
import highlightNode from './highlightNode';
import removeHighlightingOnAllNodes from './removeHighlightingOnAllNodes';
import scrollToNode from './scrollToNode';
import toggleHighlightingNodes from './toggleHighlightingNodes';
import unhighlightNode from './unhighlightNode';

export default [
  devtoolsClosed,
  highlightNode,
  removeHighlightingOnAllNodes,
  scrollToNode,
  toggleHighlightingNodes,
  unhighlightNode
];
