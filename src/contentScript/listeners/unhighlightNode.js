import { unhighlightNode } from '../actions/deprecationHighlighter';

export default attachListener => (
  attachListener('UNHIGHLIGHT_NODE', (message) => {
    const { nodeId } = message;

    unhighlightNode(nodeId);
  })
);
