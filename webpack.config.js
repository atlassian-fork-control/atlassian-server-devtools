const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const { htmlWebpackPluginConfig } = require('./webpack.config.parts');

const ENV = require('./config/env');
const PATHS = require('./config/paths');

const webpackConfig = {
  devtool: 'source-map',

  entry: {
    asd_background: `${PATHS.SRC}/background/background.js`,

    asd_content_script: `${PATHS.SRC}/contentScript/contentScript.js`,

    asd_devtools: `${PATHS.SRC}/devtools/devtools.js`,

    asd_panel: `${PATHS.SRC}/Panel/Panel.jsx`
  },

  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    path: PATHS.DIST
  },

  resolve: {
    extensions: ['.js', '.json', '.jsx']
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: 'eslint-loader'
      },

      {
        exclude: [
          /\.(html|ejs)$/,
          /\.(js|jsx)$/,
          /\.(css|less)$/,
          /\.json$/,
          /\.svg$/,
          /\.(ttf|eot|woff2?)$/,
          /\.(png|jpe?g|gif)$/
        ],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'static/media/[name].[hash:8].[ext]'
          }
        }]
      },

      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          }
        ]
      },

      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                camelCase: true
              }
            }
          ]
        })
      },

      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                camelCase: true,
                modules: true,
                localIdentName: ENV === 'development' ? '[path][name]__[local]--[hash:base64:5]' : '[hash:base64]'
              }
            },
            {
              loader: 'less-loader',
              options: {
                strictMath: true,
                sourceMap: true
              }
            }
          ]
        })
      },

      {
        test: /\.(png|jpe?g|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000,
            name: '[hash:8].[ext]'
          }
        }]
      },

      {
        test: /\.(ttf|eot|woff2?)$/,
        use: 'file-loader'
      },

      {
        test: /\.svg$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000,
            name: '[hash:8].[ext]'
          }
        }]
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(ENV)
      }
    }),

    new ExtractTextPlugin({
      filename: '[name].css',
      disable: ENV === 'development'
    }),

    new webpack.NamedModulesPlugin(),

    new webpack.NamedChunksPlugin((chunk) => {
      if (chunk.name) {
        return chunk.name;
      }

      return chunk.mapModules(m => path.relative(m.context, m.request)).join('_');
    }),

    new HtmlWebpackPlugin(htmlWebpackPluginConfig({
      title: 'ASD DevTools',
      filename: 'asd_devtools.html',
      chunks: ['asd_devtools']
    })),

    new HtmlWebpackPlugin(htmlWebpackPluginConfig({
      title: 'ASD DevTools Panel',
      filename: 'asd_panel.html',
      chunks: ['asd_panel']
    })),

    new CopyWebpackPlugin([
      {
        from: `${PATHS.SRC}/manifest.json`,
        to: PATHS.DIST
      },
      {
        from: `${PATHS.SRC}/icons/`,
        to: `${PATHS.DIST}/icons/`
      },
      {
        from: `${PATHS.SRC}/popups/`,
        to: `${PATHS.DIST}/popups/`
      }
    ]),

    new UglifyJsPlugin({
      uglifyOptions: {
        compress: {
          warnings: false,
          drop_console: ENV === 'production',
          drop_debugger: ENV === 'production'
        },
        mangle: false,
        output: {
          beautify: false,
          comments: false
        }
      },

      sourceMap: true,
      parallel: 4,
      cache: true
    }),

    new CircularDependencyPlugin({
      failOnError: true
    })
  ]
};

module.exports = webpackConfig;
